    <div class="site-container">
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
            
            <div class="menu-wrapper clearfix">
				<?php 
					print theme('links',array('links'=>$main_menu));
				?>               
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            
            <div class="featured-photo container">
	<?php
								
		print render($page['feature_image']);
								
	?>	
            </div>
            
            <!-- tabs will go here -->
            <div class="tab-container container">
            </div>
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>

						<?php if($page['left_column']): ?>            
              <div class="content inner-container clearfix shadow background">
                  <div class="left-column column region one-fourth left">
                      <div class="region-inner">
            							<div id="sidebar">
                              <?php print render($page['left_column']); ?>
            							</div>
                      </div>
                  </div>
								<?php endif; ?>	
                
                <div class="main-content three-fourths left">
                    <div class="content <?php print $variables['photo_blog_class']['content_class']?>">
                        <!-- messages will go here -->
                        <div id="messages">
                          <?php if($message){ print $messages; } ?>
                        </div>
						<?php print render($page['content']);?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
                <p><?php print render($page['footer']);?></p>
            </div>
        </div>
        
    </div>

