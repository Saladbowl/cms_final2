  <div class="site-container">
        
    <div class="menu-container inner-container">
       <a href="<?php print check_url($front_page); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    <?php print theme('links',array('links'=>$main_menu));?>
    </div>

        <!-- header -->
        <div class="header-wrapper">
        </div>
        
        <!-- content -->
        <div class="content-container container">

            
            <!-- tabs will go here -->
            <div class="tab-container container">
            </div>
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>
            
            <div class="content inner-container clearfix">
                <div class="main-content <?php print $variables["final"]["main-content-width"]; ?> left">
                    <div class="content">
                        
                        <!-- messages will go here -->
                        <div id="messages">
                        <?php print $messages; ?>
                        </div>

                <?php print render($page['content']);?>


                    </div>
                </div>

                 <?php if($page['right']): ?>
                    <div class="right-column column region">
                         <div class="region-inner">
                             <?php print render($page['right']);?>
                         </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
            <?php print render($page['footer']); ?>
            </div>
        </div>
        
    </div>