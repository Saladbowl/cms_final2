<?php
  
  function final_preprocess_page(&$variables){
    
    $page = $variables["page"];
    $variables["final"]["main-content-width"] = "three-fourths";
    
    if(empty($page["right"])){
      // we need to assign a class "full-width" to main content
      
      $variables["final"]["main-content-width"] = "full-width";
      
    }
    
  }
